1. **Activities in the lab:** *Squatting down*, *standing up*, walking, *running*, *falling, climbing*. 
2. **Lab_data.csv**: activity samples in the lab. Data dimension: 480x54000. Each row is a sample, dimension 200x270, expanded into diemension 1x54000. 480 rows, representing 6 activities, 8 locations, 10 samples per activitity per location.
3. **Lab_label.csv**: activity labels in the lab. Data dimension: 480x6. Each row corresponds to each row in Lab_data.csv, in the form of one-hot encoding.
4. **Activities in the lobby:** *squatting down*, *standing up*, *jumping*.
5. **Lobby_data.csv**: activity samples in the lobby. Data dimension: 600x54000. Each row is a sample, dimension 200x270, expanded into diemension 1x54000. 600 rows, representing 3 activities, 20 locations, 10 samples per activity per location.
6. **Lobby_label.csv**: activity labels in the lobby. Data dimension: 600x3. Each row corresponds to each row in Lobby_data.csv, in the form of one-hot encoding.
7. Note: 1. The data was collected with IWL5300.
8. Note: 2. The setup had 1 transmitter with 3 antennas, 1 receiver with 3 antennas. Each antenna pair had 30 groups of subcarriers.
10. Note: 3. All the values are CSI amplitude.
11. Note: 4. In the dataset of the lab, there are 8 locations, each location has 60 samples (6 activities x 10 times). Hence line 1-60 represents location 1, line 61-120 represents location 2, ..., and line 421-480 represents location 8. 
12. Note: 5. In the dataset of the lobby, there are 20 locations, each location has 30 samples (3 activities x 10 times). Hence line 1-30 represents location 1, line 31-60 represents location 2, ..., and line 571-600 represents location 20.
